import Adafruit_BBIO.GPIO as GPIO
import Adafruit_GPIO.SPI as SPI
from threading import Thread
import MAX31855 as MAX31855
from socket import *
from select import select
import sys
import struct
import binascii
import time

SPI_PORT   = 1
SPI_DEVICE = 0
sensor = MAX31855.MAX31855(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

#HOST = ''   # Symbolic name, meaning all available interfaces
#PORT = 8888 # Arbitrary non-privileged port
 
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#print 'Socket created'

def c_to_f(c):
        return c * 9.0 / 5.0 + 32.0


pinmap = ["P8_3","P8_5","P8_7","P8_9","P8_11",
          "P8_13","P8_15","P8_17","P8_19","P8_21",
          "P8_23","P8_25","P8_27","P8_29","P8_31",
          "P8_4","P8_6","P8_8","P8_10","P8_12",
          "P8_14","P8_16","P8_18","P8_20","P8_22",
          "P8_24","P8_26","P8_28","P8_30","P8_32"];
def All():
    for x in range (0,30):
        GPIO.output(pinmap[x], GPIO.HIGH)


def Ch(j):
    All()
    GPIO.output(pinmap[j], GPIO.LOW)

def Settup():
    for x in range (0,30):
        print(pinmap[x] + "OUTPUT Setting")
        GPIO.setup(pinmap[x], GPIO.OUT)

#Bind socket to local host and port
#try:
#    s.bind((HOST, PORT))
#except socket.error as msg:
#    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
#    sys.exit()
     
#print 'Socket bind complete'
 
#Start listening on socket
#s.listen(10)
print 'Socket now listening'

Settup()
print 'GPIO Set-up'
All()
data_temp=[0 for j in range(0,30)]
data_itemp=[0 for j in range(0,30)]
data_ctemp=[0 for j in range(0,30)]

print (sys.byteorder)
HOST = '192.168.7.2'
PORT = 4444
BUFSIZE = 4096
ADDR = (HOST, PORT)
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(ADDR)
serverSocket.listen(1000)

#values = (0xEFBEADDE, 1, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30)
packer = struct.Struct('I I f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f f')
conn, addr = serverSocket.accept()
print ('Connected with ' + addr[0] + ':' + str(addr[1]))

def timer(con, packer):
        while True:
                print("Time" + ": " + str(time.ctime(time.time())))
                time.sleep(1)
                values = []    
                values.append(0xEFBEADDE)
                values.append(1)   
                for x in range(0,30):
                        Ch(x)
                        temp = sensor.readTempC()
                        Ch(x)
                        internal = sensor.readInternalC()
                        Ch(x)
                        comp_t = sensor.readLinearizedTempC();
                        Ch(x)
                        print("CH" + str(x) + "---------------------------")
                        print('Thermocouple Temperature: {0:0.3F}*C / {1:0.3F}*F'.format(temp, c_to_f(temp)))
                        print('    Internal Temperature: {0:0.3F}*C / {1:0.3F}*F'.format(internal, c_to_f(internal)))
                        print('  Linearized Temperature: {0:0.3F}*C / {1:0.3F}*F'.format(comp_t, c_to_f(comp_t)))
                        values.append(temp)
                        values.append(internal)
                        values.append(comp_t)   
                        print(sensor.readState())
                        All()
                packed_data = packer.pack(*values)
                print ('sending "%s"' % binascii.hexlify(packed_data), values)
                conn.sendall(packed_data)
                
        
t1 = Thread(target=timer, args =(conn,packer))
t1.start()
print("Hello")


    
        
